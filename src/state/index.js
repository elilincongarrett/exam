import { createStore } from "vuex";
import VuexPersistence from 'vuex-persist'

const vuexLocal = new VuexPersistence({
    storage: window.localStorage,
    key: "pokeapi"
})
const store = new createStore({
    state() { return { selected: [] } },
    getters: {
        getSelected(state) {
            return state.selected
        }
    },
    mutations: {
        addSelected(state, val) {
            state.selected.push(val)
        }
    },
    plugins: [vuexLocal.plugin]
})
export default store
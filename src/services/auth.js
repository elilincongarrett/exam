import { initializeApp } from "firebase/app";
import { getFirestore, collection, getDocs } from 'firebase/firestore/lite';
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "firebase/auth";


const firebaseConfig = {
  apiKey: process.env.VUE_APP_API_KEY,
  authDomain: process.env.VUE_APP_AUTH_DOMAIN,
  projectId: process.env.VUE_APP_PROJECT_ID,
  storageBucket: process.env.VUE_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.VUE_APP_MESSAGING_SENDER_ID,
  appId: process.env.VUE_APP_API_ID
};

// Initialize Firebase
initializeApp(firebaseConfig);
const auth = getAuth();

//Registro
export const signUp = async (email, password) => {
  try {
    var userCredential = await createUserWithEmailAndPassword(auth, email, password)
    sessionStorage.setItem("user", userCredential.user.uid)
    sessionStorage.setItem("name", userCredential.user.email.split('@')[0])
    sessionStorage.setItem("token", userCredential.user.accessToken)
    return userCredential;
  } catch (error) {
    throw ({
      code: error.code,
      message: error.message
    })
  }
}

//auth
export const signIn = async (email, password) => {
  try {
    var userCredential = await signInWithEmailAndPassword(auth, email, password)
    sessionStorage.setItem("user", userCredential.user.uid)
    sessionStorage.setItem("name", userCredential.user.email.split('@')[0])
    sessionStorage.setItem("token", userCredential.user.accessToken)
    return userCredential;
  } catch (error) {
    throw ({
      code: error.code,
      message: error.message
    })
  }
}

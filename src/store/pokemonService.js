import axios from "axios"

const instance = axios.create({
    baseURL: 'https://pokeapi.co/api/v2'
});

export const getTypes = async () => {
    try {
        var result = await instance.get("/type")
        var items = []
        await result.data.results.forEach(async element => {
            console.log((element.url.split("/")).slice(-2)[0]);
            items.push({
                text: element.name,
                value: (element.url.split("/")).slice(-2)[0]
            })
        });
        return items;
    } catch (error) {
        console.log(error);
        throw ([])
    }
}

export const getAllPokemon = async () => {
    try {
        var result = await instance.get("/pokemon?limit=9&offset=0")

        var items = []
        await result.data.results.forEach(async element => {
            var item = await instance.get("/pokemon/" + element.name)
            items.push(item.data)
        });

        return items;
    } catch (error) {
        console.log(error);
        throw ([])
    }
}

export const getPokemonById = async (id) => {
    try {
        var result = await instance.get("/pokemon/" + id)
        return result.data;
    } catch (error) {
        console.log(error);
        throw ({})
    }
}

export const getByType = async (type) => {
    try {
        var result = await instance.get("/type/" + type + "?limit=9&offset=0")
        var list = result.data.pokemon.slice(-9)
        var items = []
        await list.forEach(async element => {
            var item = await instance.get("/pokemon/" + element.pokemon.name)
            items.push(item.data)
        });
        return items;
    } catch (error) {
        console.log(error);
        throw ([])
    }
}
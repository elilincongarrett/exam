// Imports
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    if (to.hash) return { selector: to.hash }
    if (savedPosition) return savedPosition
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '',
      name: 'Login',
      component: () => import('@/views/Login.vue'),
    },
    {
      path: '/register',
      name: 'Register',
      component: () => import('@/views/Register.vue'),
    },
    {
      path: '/',
      component: () => import('@/layouts/Index.vue'),
      children: [
        {
          path: 'home',
          name: 'Home',
          component: () => import('@/views/dashboard/Home.vue'),
        },
        {
          path: 'pokemon-detail/:name',
          name: 'Detail',
          component: () => import('@/views/dashboard/Detail.vue'),
        },
      ],
    },
  ],
})


router.beforeEach((to, from, next) => {
  var token = sessionStorage.getItem("token")
  switch (to.name) {
    case "Login":
    case "Register":
      !token ? next() : next({ name: "Home" })
      break;
    default:
      token ? next() : next({ name: "Login" })
      break;
  }
})
export default router
